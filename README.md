# a complete infra for large-scale python development



If you can use docker-compose, you can simulate complicated--but necessary--infrastructure
for your local development.

Run both of thses in this directory

`docker-compose pull`

`docker-compose up`


After, you should be able to hit all of the following:

http://localhost:8000/shell    --dynamodb

http://localhost:5601          --kibana

localhost:5432          --postgres username: pgdev password: pgdev database: pgdev

localhost:6379          --redis

localhost:2181          --zookeeper

localhost:9092          --kafka bootstrap

http://localhost:9200   --elasticsearch

tcp://localhost:5000    --logstash inbound for other application logs


the parts that make the ELK stack work were updated for the latest version from
https://github.com/Einsteinish/docker-elk 
